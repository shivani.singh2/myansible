![image](https://user-images.githubusercontent.com/19822562/146780725-e1aa6b19-486b-4a19-9b60-2ce5eff773ff.png)

![image](https://user-images.githubusercontent.com/19822562/146780776-7662344c-4673-4e0a-af15-4fa7133eb4af.png)

![image](https://user-images.githubusercontent.com/19822562/146780863-9cceebb8-864a-4cda-a041-63c78fc7cfef.png)

![image](https://user-images.githubusercontent.com/19822562/146780936-310f26c8-1a80-47fa-b56a-2da6ca51f978.png)

![image](https://user-images.githubusercontent.com/19822562/146782807-ba6e6b46-d1f2-4f48-93a2-97710607c5ce.png)

![image](https://user-images.githubusercontent.com/19822562/146783713-d991e512-cd29-4d7d-b5c0-a24973662ed7.png)

![image](https://user-images.githubusercontent.com/19822562/146783920-0e2967e5-4438-426c-9a3f-c0d789afcc2b.png)

![image](https://user-images.githubusercontent.com/19822562/146787837-78d52c45-98ad-497c-a116-11bb9f7dfd57.png)

Make API request to get indices status. Are they yellow/green? Why?

![image](https://user-images.githubusercontent.com/19822562/146789166-49a4e8be-86db-44df-86ed-2c2868757fe9.png)

Make API request to get cluster health status. What do you understand by it?
![image](https://user-images.githubusercontent.com/19822562/146789294-c942cec3-4719-4cd0-adcc-2746d2bf7690.png)

Remove one node ( don't shut it down, block In security group/firewall). See what happens to shards and replicas, why?
![image](https://user-images.githubusercontent.com/19822562/146789901-dfe36bf7-b475-43cd-aa10-411c830d0d17.png)
